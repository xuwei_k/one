package scalaz

import collection.mutable.ArrayBuffer

sealed trait NonEmpty[+A, F[+_]]{ self =>
  val head: A
  val tail: F[A]

  val builder: NonEmpty.Builder[F]

  import builder._

  type This[+A] = NonEmpty[A, F]

  def nonEmpty[B](h: B, t: Seq[B]): This[B] = ???
  def nonEmpty[B](h: B, t: F[B]): This[B] = new NonEmpty[B, F]{
    val head = h
    val tail = t
    val builder = self.builder
  }

  import Zipper._

  def <::[AA >: A](b: AA): This[AA] = nonEmpty(b, cons(head, tail))

  def <:::[AA >: A](bs: F[AA]): This[AA] = {
    val b = new ArrayBuffer[AA]
    b ++= seq(bs)
    b += head
    b ++= seq(tail)
    val bb = fromBuffer(b)
    nonEmpty(builder.head(bb), builder.tail(bb))
  }

  def :::>[AA >: A](bs: F[AA]): This[AA] = nonEmpty(head, seq(tail) ++ seq(bs))

  def append[AA >: A](f2: This[AA]): This[AA] = vector <::: f2

  def map[B](f: A => B): This[B] = nonEmpty(f(head), builder.map(tail)(f))

  def flatMap[B](f: A => This[B]): This[B] = {
    val b = new ArrayBuffer[B]
    val p = f(head)
    b += p.head
    b ++= seq(p.tail)
    foreach(tail){
      a =>
        val p = f(a)
        b += p.head
        b ++= seq(p.tail)
    }
    val bb = fromBuffer(b)
    nonEmpty(builder.head(bb), seq(builder.tail(bb)))
  }

/*
  def traverse1[F[_], B](f: A => F[B])(implicit F: Apply[F]): F[This[B]] = extract(tail) match {
    case Some((b, bs)) => F.apply2(f(head), nonEmpty(b, bs).traverse1(f)) {
      case (h, t) => nonEmpty(h, cons(head(t), tail(t)))
    }
    case None => F.map(f(head))(nonEmpty(_, Vector.empty))
  }
*/

  def vector: F[A] = cons(head, tail)

  def stream: Stream[A] = head #:: toStream(tail)

  def toZipper: Zipper[A] = zipper(Stream.Empty, head, toStream(tail))

  def zipperEnd: Zipper[A] = {
    import Stream._
    extract(builder.reverse(tail)) match {
      case Some((t, ts)) => zipper(toStream(ts) :+ head, t, Stream.empty)
      case None => zipper(Stream.empty, head, Stream.empty)
    }
  }

/*
  def tails: This[This[A]] = nonEmpty(this, extract(tail) match {
    case Some((h, t)) => nonEmpty(h, t).tails.vector
    case _ => Vector.empty
  })
  */

  def reverse: This[A] = extract(builder.reverse(vector)) match {
    case Some((x, xs)) => nonEmpty(x, xs)
  }

  def size: Int = 1 + builder.size(tail)

/*
  def zip[B](b: => This[B]): NonEmpty[(A, B)] =
    nonEmpty((head, b.head), tail zip b.tail)

  def unzip[X, Y](implicit ev: A <:< (X, Y)): (NonEmpty[X], NonEmpty[Y]) = {
    val (a, b) = head: (X, Y)
    val (aa, bb) = tail.unzip: (Vector[X], Vector[Y])
    (nonEmpty(a, aa), nonEmpty(b, bb))
  }
*/

  override def toString: String = "NonEmpty" + cons(head, tail)

  override def equals(any: Any): Boolean =
    any match {
      case that: NonEmpty[_, _] => this.vector == that.vector
      case _                     => false
    }

  override def hashCode: Int =
    vector.hashCode
}

object NonEmpty{

  trait Builder[F[+_]]{
    def fromBuffer[B](x: ArrayBuffer[B]): F[B]
    def empty[B]: F[B]
    def extract[B](a: F[B]): Option[(B, F[B])]
    def cons[B](head: B, tail: F[B]): F[B]
    def size(a: F[_]): Int
    def reverse[B](a: F[B]): F[B]
    def toStream[B](a: F[B]): Stream[B] = seq(a).toStream
    def head[B](a: F[B]): B
    def tail[B](a: F[B]): F[B]
    def foreach[B](a: F[B])(f: B => Unit): Unit = map(a)(f)
    def seq[B](a: F[B]): Seq[B]
    def map[B, C](a: F[B])(f: B => C): F[C]
  }

  implicit val nonEmptyVectorBuilder = new Builder[Vector]{
    def cons[B](head: B,tail: Vector[B]): Vector[B] = head +: tail
    def empty[B]: Vector[B] = Vector.empty
    def extract[B](a: Vector[B]): Option[(B, Vector[B])] = PartialFunction.condOpt(a){
      case h +: t => h -> t
    }
    def fromBuffer[B](x: ArrayBuffer[B]): Vector[B] = x.toVector
    def head[B](a: Vector[B]): B = a.head
    def map[B, C](a: Vector[B])(f: B => C): Vector[C] = a map f
    def reverse[B](a: Vector[B]): Vector[B] = a.reverse
    def seq[B](a: Vector[B]): Seq[B] = a.toSeq
    def size(a: Vector[_]): Int = a.size
    def tail[B](a: Vector[B]): Vector[B] = a.tail
  }

  type NonEmptyVector[+A] = NonEmpty[A, Vector]

  def nonEmpty[A, F[+_]](h: A, t: F[A])(implicit e: Builder[F]): NonEmpty[A, F] = new NonEmpty[A, F]{
    val head = h
    val tail = t
    val builder = e
  }


}
/*
object NonEmpty extends NonEmptyFunctions with NonEmptyInstances {
  def apply[A](h: A, t: A*): NonEmpty[A] =
    nonEmptys(h, t: _*)

  def unapplySeq[A](v: NonEmpty[A]): Option[(A, Vector[A])] =
    Some((v.head, v.tail))
}

trait NonEmptyInstances0 {
  implicit def NonEmptyEqual[A: Equal]: Equal[NonEmpty[A]] = Equal.equalBy[NonEmpty[A], Vector[A]](_.vector)(std.vector.vectorEqual[A])
}

trait NonEmptyInstances extends NonEmptyInstances0 {
  implicit val NonEmpty =
    new Traverse1[NonEmpty] with Monad[NonEmpty] with Plus[NonEmpty] with Comonad[NonEmpty] with Cobind.FromCojoin[NonEmpty] with Each[NonEmpty] with Zip[NonEmpty] with Unzip[NonEmpty] with Length[NonEmpty] {
      def traverse1Impl[G[_] : Apply, A, B](fa: NonEmpty[A])(f: A => G[B]): G[NonEmpty[B]] =
        fa traverse1 f

      override def foldRight1[A](fa: NonEmpty[A])(f: (A, => A) => A): A = fa.tail match {
        case h +: t => f(fa.head, foldRight1(NonEmpty.nonEmpty(h, t))(f))
        case _ => fa.head
      }

      override def foldLeft1[A](fa: NonEmpty[A])(f: (A, A) => A): A = fa.tail match {
        case h +: t => foldLeft1(NonEmpty.nonEmpty(f(fa.head, h), t))(f)
        case _ => fa.head
      }

      // would otherwise use traverse1Impl
      override def foldLeft[A, B](fa: NonEmpty[A], z: B)(f: (B, A) => B): B =
        fa.tail.foldLeft(f(z, fa.head))(f)

      def bind[A, B](fa: NonEmpty[A])(f: A => NonEmpty[B]): NonEmpty[B] = fa flatMap f

      def point[A](a: => A): NonEmpty[A] = NonEmpty(a)

      def plus[A](a: NonEmpty[A], b: => NonEmpty[A]): NonEmpty[A] = a.vector <::: b

      def copoint[A](p: NonEmpty[A]): A = p.head

      def cojoin[A](a: NonEmpty[A]): NonEmpty[NonEmpty[A]] = a.tails

      def each[A](fa: NonEmpty[A])(f: A => Unit) = fa.vector foreach f

      def zip[A, B](a: => NonEmpty[A], b: => NonEmpty[B]) = a zip b

      def unzip[A, B](a: NonEmpty[(A, B)]) = a.unzip

      def length[A](a: NonEmpty[A]): Int = a.size
    }

  implicit def NonEmptySemigroup[A]: Semigroup[NonEmpty[A]] = new Semigroup[NonEmpty[A]] {
    def append(f1: NonEmpty[A], f2: => NonEmpty[A]) = f1 append f2
  }

  implicit def NonEmptyShow[A: Show]: Show[NonEmpty[A]] = new Show[NonEmpty[A]] {
    import std.vector._
    override def show(fa: NonEmpty[A]) = Show[Vector[A]].show(fa.vector)
  }

  implicit def NonEmptyOrder[A: Order]: Order[NonEmpty[A]] =
    Order.orderBy[NonEmpty[A], Vector[A]](_.vector)(std.vector.vectorOrder[A])
}

trait NonEmptyFunctions {
  def nonEmpty[A](h: A, t: Seq[A]): NonEmpty[A] = new NonEmpty[A] {
    val head = h
    val tail = t.toVector
  }

  def nonEmptys[A](h: A, t: A*): NonEmpty[A] =
    nonEmpty(h, t.toVector)
}
*/
