scalaVersion := "2.10.1"

libraryDependencies ++= {
val scalazV = "7.0.0-RC2"
Seq(
  "org.scalaz" %% "scalaz-core" % scalazV,
  "org.scalaz" %% "scalaz-scalacheck-binding" % scalazV,
  "org.typelevel" %% "scalaz-specs2" % "0.1.3" % "test"
)
}

